function generateSlug(str) {
  return new Promise((resolve, reject) => {
    if (!str) {
      return reject("String is required");
    }
    let splitStr = str.replace(/[()]/g, "").toLowerCase().split(" ");
    for (var i = 0; i < splitStr.length; i++) {
      splitStr[i] =
        splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
    }
    resolve(splitStr.join("-"));
  });
}

module.exports = generateSlug;
