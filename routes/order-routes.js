const { Router } = require("express");
const router = Router();
const Product = require("../models/product");
const Order = require("../models/order");
const sgMail = require("@sendgrid/mail");
const auth = require("../middleware/auth");

const createMailTemplate = require("../modules/createMailTemplate");

router.post("/create", (req, res) => {
  const productID = req.body.productID;
  if (!req.body.email || !req.body.name) {
    return res.status(500).send("Required fields are missing");
  }
  if (!productID || productID.length === 0) {
    return res.status(400).send("Add atleast one product to place an order");
  }
  sgMail.setApiKey(process.env.SENDGRID_APIKEY);
  Product.find(
    {
      _id: {
        $in: productID,
      },
    },
    async (err, products) => {
      if (err) {
        console.log(err);
        return res.status(500).send(err);
      }

      let productTotal = 0;
      let productToCart = [];
      let productsAdded = products.map((product) => {
        productTotal = productTotal + product.price;
        const { name, price, _id: productID } = product;
        productToCart.push({ productID });
        return {
          name,
          price,
        };
      });

      const order = new Order({
        name: req.body.name,
        email: req.body.email,
        products: productToCart,
        total: productTotal,
      });

      try {
        await order.save();
      } catch (error) {
        console.log(error);
        return res.status(500).send("Unable to save the order");
      }

      let messageHTML = await createMailTemplate(productsAdded, productTotal);
      const msg = {
        to: req.body.email,
        from: "orders@codemaster.online",
        subject: "Order Status - ProductX",
        html: messageHTML,
      };
      sgMail.send(msg).then(
        () => {
          res.send();
        },
        (error) => {
          console.error(error);
          console.log(error.response.body.errors);
          res.status(500).send();
        }
      );
    }
  );
});

router.get("/get/all", auth, (req, res) => {
  Order.find()
    .populate("products.productID")
    .exec((err, orders) => {
      if (err) {
        return res.status(500).send(err);
      }
      res.send(orders);
    });
});

module.exports = router;
