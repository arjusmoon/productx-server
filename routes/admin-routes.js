const { Router } = require("express");
const router = Router();

const Admin = require("../models/admin");
const auth = require("../middleware/auth");
const { route } = require("./order-routes");

router.post("/create", async (req, res) => {
  const { name, email, password } = req.body;
  if (!name || !email || !password) {
    return res.status(400).send("Required parameters are missing.");
  }
  const admin = new Admin({
    name,
    email,
    password,
  });

  try {
    await admin.save();
    const token = await admin.generateAuthToken();
    res.send(token);
  } catch (error) {
    res.status(500).send(error);
  }
});

router.post("/login", async (req, res) => {
  try {
    const admin = await Admin.findByCredentials(
      req.body.email,
      req.body.password
    );
    if (!admin) {
      throw new Error("Admin not found");
    }
    const token = await admin.generateAuthToken();
    res.send({ admin: admin.getPublicProfile(), token });
  } catch (error) {
    console.log(error);
    res.status(500).send("Unable to login. Email / Password is incorrect");
  }
});

router.post("/logout", auth, async (req, res) => {
  try {
    req.admin.tokens = req.admin.tokens.filter((token) => {
      return token.token !== req.token;
    });
    await req.admin.save();
    res.send();
  } catch (error) {
    console.log(error);
    res.status(500).send();
  }
});

router.post("/validate-token", auth, (req, res) => {
  res.send({ valid: true });
});

module.exports = router;
