const { Router } = require("express");
const router = Router();
const path = require("path");
const fs = require("fs");

const multer = require("multer");

const Product = require("../models/product");
const auth = require("../middleware/auth");
const generateSlug = require("../modules/slug-generator");

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "./product-images");
  },
  filename: function (req, file, cb) {
    if (!file.originalname.match(/\.(png|jpg|jpeg)$/)) {
      return cb(new Error("Please upload an image file."));
    }
    var ext = "";
    if (file.mimetype == "image/png") {
      ext = ".png";
    } else if (file.mimetype == "image/jpeg") {
      ext = ".jpg";
    }
    cb(null, file.fieldname + "-" + Date.now() + ext);
  },
});
var upload = multer({ storage: storage });

router.post(
  "/create",
  auth,
  upload.single("product-image"),
  async (req, res) => {
    if (!req.body.name) {
      return res.status(400).send("Product name is required");
    }
    const slug = await generateSlug(req.body.name);
    console.log(slug);
    const product = new Product({
      name: req.body.name,
      description: req.body.description,
      price: req.body.price,
      image: req.file.filename,
      slug,
    });

    try {
      await product.save();
      res.send(product);
    } catch (error) {
      console.log(error);
      res.status(500).send(error);
    }
  },
  (error, req, res, next) => {
    res.status(400).send("Image upload error");
  }
);

router.get("/get", (req, res) => {
  Product.find().exec((err, products) => {
    if (err) {
      return res.status(500).send(err);
    }
    res.send(products);
  });
});

router.get("/get/:slug", (req, res) => {
  Product.findOne({ slug: req.params.slug }).exec((err, product) => {
    if (err) {
      return res.status(404).send(err);
    }
    res.send(product);
  });
});

router.get("/get/product-image/:id", (req, res) => {
  const productImageFile = req.params.id;
  if (
    fs.existsSync(path.join(__dirname, "../product-images/" + productImageFile))
  ) {
    res.sendFile(path.join(__dirname, "../product-images/" + productImageFile));
  } else {
    res.status(404).send("File doesn't exist");
  }
});

router.post("/get/cart", (req, res) => {
  if (!req.body.products || req.body.products.length === 0) {
    return res.status(500).send("Product IDs are missing");
  }
  Product.find(
    {
      _id: {
        $in: req.body.products,
      },
    },
    (err, products) => {
      if (err) {
        console.log(err);
        return res.status(500).send(err);
      }
      res.send(products);
    }
  );
});

module.exports = router;
