const express = require("express");
const app = express();
const { mongoose } = require("./db/mongoose");
const cors = require("cors");
const bodyParser = require("body-parser");

const port = process.env.PORT;
const productRoutes = require("./routes/product-routes");
const adminRoutes = require("./routes/admin-routes");
const orderRoutes = require("./routes/order-routes");

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use("/api/products", productRoutes);
app.use("/api/admin", adminRoutes);
app.use("/api/orders", orderRoutes);

app.listen(port, () => {
  console.log(`Server started on port ${port}`);
});
