const mongoose = require("mongoose");
var productSchema = new mongoose.Schema({
  name: {
    type: String,
    trim: true,
    required: true,
    unique: true,
    index: true,
  },
  description: {
    type: String,
    required: true,
  },
  price: {
    type: Number,
    required: true,
  },
  image: {
    type: String,
    required: true,
  },
  slug: {
    type: String,
    required: true,
  },
});

productSchema.set("timestamps", true);

const Product = mongoose.model("products", productSchema);

module.exports = Product;
