const mongoose = require("mongoose");
const validator = require("validator");

var orderSchema = new mongoose.Schema({
  name: {
    type: String,
    trim: true,
    required: true,
  },
  email: {
    type: String,
    required: true,
    trim: true,
    lowercase: true,
    validate(value) {
      if (!validator.isEmail(value)) {
        throw new Error("Email is invalid");
      }
    },
  },
  products: [
    {
      productID: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: "products",
      },
    },
  ],
  total: {
    type: Number,
    required: true,
  },
});

orderSchema.set("timestamps", true);

const Order = mongoose.model("orders", orderSchema);

module.exports = Order;
